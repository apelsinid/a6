import java.util.*;

// Inspiration:
// https://stackoverflow.com/questions/12886036/deep-copying-a-graph-structure
// https://stackoverflow.com/questions/25147565/serializing-java-object-without-stackoverflowerror

/** Container class to different classes, that makes the whole
 * set of classes one class formally.
 *
 * My assignment is to implement clone method to Graph class in order to return deep copy of Graph instance.
 */
public class GraphTask {

   /** Main method. */
   public static void main (String[] args) {
      GraphTask a = new GraphTask();
      a.run();
   }

   /** Actual main method to run examples and everything. */
   public void run() {
//      // Simple demo
//      Graph g = new Graph("G");
//      g.createRandomSimpleGraph (10, 9);
//      System.out.println (g);
//      Graph h = g.clone();
//      System.out.println(h);

//      // Small clone with all IDs prefixed to prove deep copy
//      Graph smallGraph = new Graph("Small");
//      smallGraph.createRandomSimpleGraph (5, 4);
//      Graph smallGraphCopy = smallGraph.clone();
//      smallGraphCopy.appendSuffix(" COPY");
//      System.out.println(smallGraph);
//      System.out.println(smallGraphCopy);

      // Large
      System.out.println ("Large graph = 2000 vertices; 1999 arcs");
      long largeStartTime = System.currentTimeMillis();
      Graph largeGraph = new Graph ("Large");
      largeGraph.createRandomSimpleGraph (2000, 1999);
      long largeGenerateTime = System.currentTimeMillis();
      Graph largeGraphClone = largeGraph.clone();
      long largeClonedTime = System.currentTimeMillis();
      System.out.println ("Large graph: time elapsed generating: " + (largeGenerateTime-largeStartTime) + " ms");
      System.out.println ("Large graph: time elapsed cloning: " + (largeClonedTime-largeGenerateTime) + " ms");
      System.out.println ("Large graph: time elapsed total: " + (largeClonedTime-largeStartTime) + " ms");

//      // Empty
//      Graph emptyGraph = new Graph("Empty");
//      System.out.println(emptyGraph);
//      Graph copyOfEmptyGraph = emptyGraph.clone();
//      System.out.println(copyOfEmptyGraph);
//      copyOfEmptyGraph.id = "Empty clone";
//      System.out.println(copyOfEmptyGraph);
//      System.out.println(emptyGraph);

//      // Small
//      Graph smallGraph = new Graph("Small");
//      smallGraph.createRandomSimpleGraph (5, 4);
//      Graph smallGraphCopy = smallGraph.clone();
//      smallGraphCopy.id = "Small graph copy";
//      System.out.println(smallGraph);
//      System.out.println(smallGraphCopy);


//      // vertex cloning
//      Vertex originalVertexA = new Vertex("Original vertex A", null, null);
//      Vertex originalVertexB = new Vertex("Original vertex B", originalVertexA, null);
//      Vertex copyOfVertexB = originalVertexB.clone();
//
//      Vertex originalVertexC = new Vertex("Original vertex C", null, null);
//      copyOfVertexB.next = originalVertexC;
//
//      System.out.println(originalVertexB);
//      System.out.println(originalVertexB.next);
//
//      System.out.println(copyOfVertexB);
//      System.out.println(copyOfVertexB.next);
   }

   /**
    * Vertex class represents graph vertex
    */
   class Vertex {

      private String id;
      private Vertex next;
      private Arc first;
      private int info = 0;
      // You can add more fields, if needed

      /**
       * @param s ID of the vertex
       * @param v Next vertex
       * @param e First arc
       */
      Vertex (String s, Vertex v, Arc e) {
         id = s;
         next = v;
         first = e;
      }

      /**
       * @param s ID of the vertex
       */
      Vertex (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

      @Override
      public Vertex clone() {
         return new Vertex(this.id, this.next, this.first);
      }
   }


   /** Arc represents one arrow in the graph. Two-directional edges are
    * represented by two Arc objects (for both directions).
    */
   class Arc {

      private String id;
      private Vertex target;
      private Arc next;
      private int info = 0;
      // You can add more fields, if needed

      Arc (String s, Vertex targetVertex, Arc nextArc) {
         id = s;
         target = targetVertex;
         next = nextArc;
      }

      Arc (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

      @Override
      public Arc clone() {
         return new Arc(this.id, this.target, this.next);
      }
   } 


   class Graph {

      private String id;
      private Vertex first;
      private int info = 0;
      // You can add more fields, if needed

      /**
       * @param s ID of the Graph
       * @param s First vertex of the Graph
       */
      Graph (String s, Vertex v) {
         id = s;
         first = v;
      }

      /**
       * @param s ID of the Graph
       */
      Graph (String s) {
         this (s, null);
      }

      @Override
      public String toString() {
         String nl = System.getProperty ("line.separator");
         StringBuffer sb = new StringBuffer (nl);
         sb.append (id);
         sb.append (nl);
         Vertex v = first;
         while (v != null) {
            sb.append (v.toString());
            sb.append (" -->");
            Arc a = v.first;
            while (a != null) {
               sb.append (" ");
               sb.append (a.toString());
               sb.append (" (");
               sb.append (v.toString());
               sb.append ("->");
               sb.append (a.target.toString());
               sb.append (")");
               a = a.next;
            }
            sb.append (nl);
            v = v.next;
         }
         return sb.toString();
      }

      /**
       * Factory method used in Random tree generation
       * Impure method, use with caution
       *
       * @param vid ID of the Vertex
       */
      public Vertex createVertex (String vid) {
         Vertex res = new Vertex (vid);
         res.next = first;
         first = res;
         return res;
      }

      /**
       * Factory method to create an arc between vertices
       *
       * @param aid ID of the Arc
       * @param from Source Vertex
       * @param to Destination Vertex
       */
      public Arc createArc (String aid, Vertex from, Vertex to) {
         Arc res = new Arc (aid);
         res.next = from.first;
         from.first = res;
         res.target = to;
         return res;
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       * @param n number of vertices added to this graph
       */
      public void createRandomTree (int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex [n];
         for (int i = 0; i < n; i++) {
            varray [i] = createVertex ("v" + String.valueOf(n-i));
            if (i > 0) {
               int vnr = (int)(Math.random()*i);
               createArc ("a" + varray [vnr].toString() + "_"
                  + varray [i].toString(), varray [vnr], varray [i]);
               createArc ("a" + varray [i].toString() + "_"
                  + varray [vnr].toString(), varray [i], varray [vnr]);
            } else {}
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int [info][info];
         v = first;
         while (v != null) {
            int i = v.info;
            Arc a = v.first;
            while (a != null) {
               int j = a.target.info;
               res [i][j]++;
               a = a.next;
            }
            v = v.next;
         }
         return res;
      }

      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       * @param n number of vertices
       * @param m number of edges
       */
      public void createRandomSimpleGraph (int n, int m) {
         if (n <= 0)
            return;
         if (n > 2500)
            throw new IllegalArgumentException ("Too many vertices: " + n);
         if (m < n-1 || m > n*(n-1)/2)
            throw new IllegalArgumentException 
               ("Impossible number of edges: " + m);
         first = null;
         createRandomTree (n);       // n-1 edges created here
         Vertex[] vert = new Vertex [n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges
         while (edgeCount > 0) {
            int i = (int)(Math.random()*n);  // random source
            int j = (int)(Math.random()*n);  // random target
            if (i==j) 
               continue;  // no loops
            if (connected [i][j] != 0 || connected [j][i] != 0) 
               continue;  // no multiple edges
            Vertex vi = vert [i];
            Vertex vj = vert [j];
            createArc ("a" + vi.toString() + "_" + vj.toString(), vi, vj);
            connected [i][j] = 1;
            createArc ("a" + vj.toString() + "_" + vi.toString(), vj, vi);
            connected [j][i] = 1;
            edgeCount--;  // a new edge happily created
         }
      }

      public void appendSuffix(String suffix) {
         this.id += suffix;
         Vertex v = first;
         while (v != null) {
            v.id += suffix;
            Arc a = v.first;
            while (a != null) {
               a.id += suffix;
               a = a.next;
            }
            v = v.next;
         }
      }

      /**
       * Traversing over Graph and cloning all vertices and arcs
       *
       * @return Deep copied clone of the instance
       */
      @Override
      public Graph clone() {
         Graph copy = new Graph(id);
         if (first == null) {
            return copy;
         }
         // Hashmap for fast lookup by Id String given the Ids are unique
         Map<String, Vertex> copiesVertices = new HashMap<>();

         // Create all copies of vertices
         Vertex vertex = first;
         Vertex copyVertex = vertex.clone();
         copy.first = copyVertex;
         copiesVertices.put(copyVertex.id, copyVertex);

         vertex = vertex.next;
         while (vertex != null) {
            copyVertex.next = vertex.clone();
            vertex = vertex.next;
            copyVertex = copyVertex.next;
            copiesVertices.put(copyVertex.id, copyVertex);
         }

         // Reset vertices
         copyVertex = copy.first;
         vertex = first;

         while (vertex != null) {
            Arc arc = vertex.first;
            if (arc == null) {
               vertex = vertex.next;
               continue;
            }

            Arc copyArc = arc.clone();
            copyArc.target = copiesVertices.get(arc.target.id);

            copyVertex.first = copyArc;

            arc = arc.next;
            while (arc != null) {
               // copy the next arc of the vertex until there is no more
               copyArc.next = arc.clone();
               copyArc.next.target = copiesVertices.get(arc.target.id);

               copyArc = copyArc.next;
               arc = arc.next;
            }
            copyVertex = copyVertex.next;
            vertex = vertex.next;
         }
         return copy;
      }
   }

} 

